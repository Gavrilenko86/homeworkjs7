function filterBy(arr, dataType) {
    if(!Array.isArray(arr)) {
      throw new Error('Перший аргумент має бути масивом!');
    }
  
    if(typeof dataType !== 'string') {
      throw new Error('Другий аргумент має бути рядком!');
    }
  
    return arr.filter(function(item) {
      return typeof item !== dataType;
    });
  }
  
  
  const data = ['hello', 'world', 23, '23', null];
  const filteredData = filterBy(data, 'string');
  console.log(filteredData); 
  
  const data2 = ['a', 'b', 'c', 1, 2, 3, null];
  const filteredData2 = filterBy(data2, 'number');
  console.log(filteredData2); 
  
  const data3 = ['foo', 42, { name: 'John' }, true, undefined];
  const filteredData3 = filterBy(data3, 'object');
  console.log(filteredData3); 
  